<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ca_ES">
<context>
    <name>DukeWindow</name>
    <message>
        <location filename="../src/dukewindow.cpp" line="27"/>
        <source>DukeTrayem configuration</source>
        <translation>Configuració de DukeTrayem</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="49"/>
        <source>Type a sentence here to add it to the list</source>
        <translation>Escriu una frase aquí per afegir-la a la llista</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="52"/>
        <source>&amp;Add</source>
        <translation>&amp;Afegir</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="60"/>
        <source>These are the manually added sentences</source>
        <translation>Aquestes son les frases afegides manualment</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="68"/>
        <source>&amp;Remove</source>
        <translation>&amp;Treure</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="77"/>
        <source>Custom</source>
        <translation>Personalitzades</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="82"/>
        <source>Sentence types</source>
        <translation>Tipus de frases</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="88"/>
        <location filename="../src/dukewindow.cpp" line="93"/>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <source>Intervals</source>
        <translation type="obsolete">Intervals</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="100"/>
        <source>Intervals (min/max)</source>
        <translation>Intervals (mín/màx)</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="107"/>
        <source>sec</source>
        <translation>seg</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="113"/>
        <source>Min. Duration</source>
        <translation>Dur. Mínima</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="119"/>
        <source>&amp;Hide</source>
        <translation>A&amp;magar</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="122"/>
        <source>&amp;Quit</source>
        <translation>&amp;Sortir</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="130"/>
        <source>Configure...</source>
        <translation>Configurar...</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="134"/>
        <source>About...</source>
        <translation>Sobre...</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="138"/>
        <source>Quit</source>
        <translation>Sortir</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="144"/>
        <source>Duke Nukem is watching you...</source>
        <translation>Duke Nukem t&apos;està vigilant...</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="329"/>
        <source>About DukeTrayem</source>
        <translation>Sobre DukeTrayem</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="332"/>
        <source>This program puts a Duke Nukem icon on your system tray, and will speak to you from time to time.

You can add your own sentences, and use it to remind you about things, for instance.</source>
        <translation>Aquest programa col·loca una icona de Duke Nukem a la teva safata del sistema, i et parlarà de tant en tant.

Pots afegir les teves pròpies frases, i fer-ho servir perque et recordi coses, per exemple.</translation>
    </message>
</context>
</TS>
