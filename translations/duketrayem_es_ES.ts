<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>DukeWindow</name>
    <message>
        <location filename="../src/dukewindow.cpp" line="27"/>
        <source>DukeTrayem configuration</source>
        <translation>Configuración de DukeTrayem</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="49"/>
        <source>Type a sentence here to add it to the list</source>
        <translation>Escribe una frase aquí para añadirla a la lista</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="52"/>
        <source>&amp;Add</source>
        <translation>&amp;Añadir</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="60"/>
        <source>These are the manually added sentences</source>
        <translation>Estas son las frases añadidas manualmente</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="68"/>
        <source>&amp;Remove</source>
        <translation>&amp;Eliminar</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="77"/>
        <source>Custom</source>
        <translation>Personalizadas</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="82"/>
        <source>Sentence types</source>
        <translation>Tipos de frases</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="88"/>
        <location filename="../src/dukewindow.cpp" line="93"/>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <source>Intervals</source>
        <translation type="obsolete">Intervalos</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="100"/>
        <source>Intervals (min/max)</source>
        <translation>Intervalos (mín/máx)</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="107"/>
        <source>sec</source>
        <translation>seg</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="113"/>
        <source>Min. Duration</source>
        <translation>Dur. Mínima</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="119"/>
        <source>&amp;Hide</source>
        <translation>Es&amp;conder</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="122"/>
        <source>&amp;Quit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="130"/>
        <source>Configure...</source>
        <translation>Configurar...</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="134"/>
        <source>About...</source>
        <translation>Acerca de...</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="138"/>
        <source>Quit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="144"/>
        <source>Duke Nukem is watching you...</source>
        <translation>Duke Nukem te está vigilando...</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="329"/>
        <source>About DukeTrayem</source>
        <translation>Acerca de DukeTrayem</translation>
    </message>
    <message>
        <location filename="../src/dukewindow.cpp" line="332"/>
        <source>This program puts a Duke Nukem icon on your system tray, and will speak to you from time to time.

You can add your own sentences, and use it to remind you about things, for instance.</source>
        <translation>Este programa coloca un icono de Duke Nukem en tu bandeja del sistema, y te hablará de vez en cuando.

Puedes añadir tus propias frases, y utilizarlo para que te recuerde cosas, por ejemplo.</translation>
    </message>
</context>
</TS>
