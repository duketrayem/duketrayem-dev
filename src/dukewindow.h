/*
 *   This file is part of DukeTrayem
 *   Copyright 2011-2015 JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QSettings>
#include <QCloseEvent>
#include <QSystemTrayIcon>
#include <QTimer>
#include <QString>
#include <QStringList>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QListWidget>
#include <QGroupBox>
#include <QCheckBox>
#include <QSpinBox>
#include <QMenu>
#include <QMessageBox>
#include <QDebug>


class DukeWindow : public QWidget
{
    Q_OBJECT

public:
    DukeWindow(QWidget *parent = 0);
    ~DukeWindow();

public slots:
    void toggleConfigDialog(QSystemTrayIcon::ActivationReason);
    void speak();

    void addSentence();
    void removeSentence();

    void loadSentences();

    void showAboutDialog();


protected:
    void closeEvent(QCloseEvent *event);


private:
    QSystemTrayIcon *trayIcon;
    QTimer *timer;

    QMenu *trayMenu;

    QStringList sentences;
    int currentSentence;


    // Widgets on the config dialog
    QVBoxLayout *mainLayout;
    QHBoxLayout *topLayout;
    QHBoxLayout *middleLayout;
    QHBoxLayout *middleDownLayout;
    QHBoxLayout *bottomLayout;

    QLineEdit *newSentence;
    QListWidget *addedSentences;

    QGroupBox *sentenceTypes;
    QHBoxLayout *sentenceTypesLayout;
    QCheckBox *useDukeSentences;
    QCheckBox *useCustomSentences;

    QGroupBox *intervals;
    QHBoxLayout *intervalsLayout;
    QSpinBox *minInterval;
    QSpinBox *maxInterval;


    QGroupBox *duration;
    QHBoxLayout *durationLayout;
    QSpinBox *minDuration;



    QPushButton *addSentenceButton;
    QPushButton *removeSentenceButton;


    QPushButton *hideButton;
    QPushButton *quitButton;

};

#endif // MAINWINDOW_H
