/*
 *   This file is part of DukeTrayem
 *   Copyright 2011-2015 JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "dukewindow.h"


DukeWindow::DukeWindow(QWidget *parent) :   QWidget(parent)
{
    QSettings settings;
    this->setWindowTitle(tr("DukeTrayem configuration"));
    this->setWindowIcon(QIcon(":/images/duke.png"));
    this->resize(settings.value("size", QSize(380, 340)).toSize());

    mainLayout = new QVBoxLayout();
    this->setLayout(mainLayout);

    topLayout = new QHBoxLayout();
    topLayout->setAlignment(Qt::AlignTop);
    middleLayout = new QHBoxLayout();
    middleLayout->setAlignment(Qt::AlignTop);
    middleDownLayout = new QHBoxLayout();
    middleDownLayout->setAlignment(Qt::AlignCenter);
    bottomLayout = new QHBoxLayout();
    bottomLayout->setAlignment(Qt::AlignBottom | Qt::AlignRight);

    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(middleLayout);
    mainLayout->addLayout(middleDownLayout);
    mainLayout->addLayout(bottomLayout);

    newSentence = new QLineEdit(this);
    newSentence->setToolTip(tr("Type a sentence here to add it to the list"));
    connect(newSentence, SIGNAL(returnPressed()), this, SLOT(addSentence()));
    topLayout->addWidget(newSentence);
    addSentenceButton = new QPushButton(QIcon::fromTheme("list-add"), tr("&Add"));
    connect(addSentenceButton, SIGNAL(clicked()), this, SLOT(addSentence()));
    topLayout->addWidget(addSentenceButton);



    // List of user-added sentences
    addedSentences = new QListWidget(this);
    addedSentences->setToolTip(tr("These are the manually added sentences"));
    QStringList savedSentences;
    savedSentences = settings.value("addedSentences", QStringList()).toStringList();
    for (int counter = 0; counter != savedSentences.length(); ++counter)
    {
        addedSentences->addItem(savedSentences.at(counter));
    }
    middleLayout->addWidget(addedSentences);
    removeSentenceButton = new QPushButton(QIcon::fromTheme("list-remove"), tr("&Remove"));
    connect(removeSentenceButton, SIGNAL(clicked()), this, SLOT(removeSentence()));
    middleLayout->addWidget(removeSentenceButton);



    // Groupbox with checkboxes to select which types of sentences to use
    useDukeSentences = new QCheckBox("Duke Nukem", this);
    useDukeSentences->setChecked(settings.value("useDukeSentences", true).toBool());
    useCustomSentences = new QCheckBox(tr("Custom"), this);
    useCustomSentences->setChecked(settings.value("useCustomSentences", false).toBool());
    sentenceTypesLayout = new QHBoxLayout();
    sentenceTypesLayout->addWidget(useDukeSentences);
    sentenceTypesLayout->addWidget(useCustomSentences);
    sentenceTypes = new QGroupBox(tr("Sentence types"), this);
    sentenceTypes->setLayout(sentenceTypesLayout);
    middleDownLayout->addWidget(sentenceTypes);

    // Groupbox with spinboxes to select time intervals
    minInterval = new QSpinBox();
    minInterval->setSuffix(" " + tr("min"));
    minInterval->setMinimum(0);
    minInterval->setMaximum(24 * 60); // min every 24 hours
    minInterval->setValue(settings.value("minInterval", 2).toInt());
    maxInterval = new QSpinBox();
    maxInterval->setSuffix(" " +  tr("min"));
    maxInterval->setMinimum(1);
    maxInterval->setMaximum(24 * 60); // max every 24 hours
    maxInterval->setValue(settings.value("maxInterval", 30).toInt());
    intervalsLayout = new QHBoxLayout();
    intervalsLayout->addWidget(minInterval);
    intervalsLayout->addWidget(maxInterval);
    intervals = new QGroupBox(tr("Intervals (min/max)"), this);
    intervals->setLayout(intervalsLayout);
    middleDownLayout->addWidget(intervals);


    // Groupbox with spinbox to select minimum duration of the popup
    minDuration = new QSpinBox();
    minDuration->setSuffix(" " + tr("sec"));
    minDuration->setMinimum(0);
    minDuration->setMaximum(30);
    minDuration->setValue(settings.value("minDuration", 0).toInt());
    durationLayout = new QHBoxLayout();
    durationLayout->addWidget(minDuration);
    duration = new QGroupBox(tr("Min. Duration"), this);
    duration->setLayout(durationLayout);
    middleDownLayout->addWidget(duration);


    // lower side buttons, to hide and quit
    hideButton = new QPushButton(QIcon::fromTheme("arrow-down-double"), tr("&Hide"), this);
    connect(hideButton, SIGNAL(clicked()), this, SLOT(hide()));
    bottomLayout->addWidget(hideButton);
    quitButton = new QPushButton(QIcon::fromTheme("application-exit"), tr("&Quit"), this);
    connect(quitButton, SIGNAL(clicked()), this, SLOT(close()));
    bottomLayout->addWidget(quitButton);



    trayMenu = new QMenu("contextMenu", this);
    trayMenu->addAction(QIcon::fromTheme("configure"),
                        tr("Configure..."),
                        this,
                        SLOT(show()));
    trayMenu->addAction(QIcon::fromTheme("help-about"),
                        tr("About..."),
                        this,
                        SLOT(showAboutDialog()));
    trayMenu->addAction(QIcon::fromTheme("application-exit"),
                        tr("Quit"),
                        this,
                        SLOT(close()));

    this->trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon(QIcon(":/images/duke.png"));
    trayIcon->setToolTip(tr("Duke Nukem is watching you..."));
    trayIcon->setContextMenu(trayMenu);
    trayIcon->show();

    // show main window (configuration) when clicking on Duke's tray icon, or context menu
    connect(this->trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(toggleConfigDialog(QSystemTrayIcon::ActivationReason)));

    loadSentences();

    currentSentence = 0;

    this->timer = new QTimer(this);
    timer->setInterval(30000);      // first message in 30 seconds
    connect(this->timer, SIGNAL(timeout()), this, SLOT(speak()));
    timer->start();
    qDebug() << "DukeWindow created";
}


DukeWindow::~DukeWindow()
{
    qDebug() << "DukeWindow destroyed";
}


/*
 * Save configuration upon exit
 */
void DukeWindow::closeEvent(QCloseEvent *event)
{
    QSettings settings;
    settings.setValue("size", this->size());

    QStringList savedSentences;
    for (int counter = 0; counter != addedSentences->count(); ++counter)
    {
        savedSentences.append(addedSentences->item(counter)->text());
    }
    settings.setValue("addedSentences", savedSentences);

    settings.setValue("useDukeSentences",   useDukeSentences->isChecked());
    settings.setValue("useCustomSentences", useCustomSentences->isChecked());

    settings.setValue("minInterval", minInterval->value());
    settings.setValue("maxInterval", maxInterval->value());

    settings.setValue("minDuration", minDuration->value());

    qDebug() << "closeEvent(): Config saved";
    event->accept();
}


/*
 * Called when clicking on tray's icon... show or hide configuration window
 */
void DukeWindow::toggleConfigDialog(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::Context)  // if context menu requested (RMB usually)
    {
        qDebug() << "tray context menu!!";
    }
    else  // if the icon was simply clicke
    {
        if (this->isHidden())
        {
            this->show();
        }
        else
        {
            this->hide();
        }
    }
}


void DukeWindow::speak()
{
    currentSentence = qrand() % sentences.length();
    qDebug() << "currentSentence:" << currentSentence;
    if (currentSentence >= sentences.length())
    {
        currentSentence = 0;
        qDebug() << "currentSentence out of bounds!";
    }


    trayIcon->showMessage("Duke:",
                          sentences.at(currentSentence),
                          QSystemTrayIcon::NoIcon,
                          (minDuration->value() * 1000)
                        + (sentences.at(currentSentence).length() * 50));


    int nextshot = minInterval->value() * 60000;
    nextshot += qrand() % ((maxInterval->value() * 60000) - nextshot);

    this->timer->setInterval(nextshot);
    qDebug() << "Next shot: " << timer->interval() << "millisecs ->" << timer->interval() / 60000.0 << "min";
    qDebug() << "min Duration: " << minDuration->value() * 1000;
}



void DukeWindow::addSentence()
{
    if (!newSentence->text().isEmpty())
    {
        addedSentences->addItem(newSentence->text());
        qDebug() << "added:" << newSentence->text();
        newSentence->clear();
        newSentence->setFocus();
    }
}


void DukeWindow::removeSentence()
{
    if (addedSentences->currentRow() != -1)  // -1 means there are no items or none selected
    {
        qDebug() << "removing item at line:" << addedSentences->currentRow();
        qDebug() << addedSentences->currentItem()->text();
        addedSentences->takeItem(addedSentences->currentRow());
        qDebug() << "count after:" << addedSentences->count();
    }
}


/*
 * Load or reload the sentences Duke's and/or custom, depending on configuration
 *
 */
void DukeWindow::loadSentences()
{
    sentences.clear();

    if (this->useDukeSentences->isChecked())
    {
        sentences << "Rockin'"
                  << "LET'S ROCK!"
                  << "Rest in... peaces!"
                  << "Looks like cleanup in aisle four..."
                  << "He he heee... what a mess..."
                  << "Ooooh, that's gotta hurt!!"
                  << "Come get sooome!"
                  << "Your face, your ass... what's the difference?"
                  << "It's my way or... hell, it's MY way!!"
                  << "Babes, bullets, bombs... Damn, I love this job!"
                  << "Half man, half animal... All DEAD!!"
                  << "Yeeeah, piece of cake!"
                  << "Who wants some?"
                  << "Now you see me... now you're DEAD!"
                  << "It is time to kick ass and chew bubblegum... and I'm aaall out of gum..."
                  << "DAMN, I'M GOOD!"
                  << "I'm gonna get medieval on your asses!"
                  << "Shit happens..."
                  << "HOLY COW!!"
                  << "End of the line... Last stop: Total destruction!!"
                  << "All aboard the midtown express... to HELL!"
                  << "Light's up, bitch, and then you die!"
                  << "Diiie, you son of a bitch!"
                  << "What're you waiting for? Christmas?"
                  << "There's only two ways this can end... and in both of them, you die!"
                  << "Game over!!"
                  << "See you in HELL!!"
                  << "I hate to kick my own ass, but it's gotta be done!";
    }


    if (this->useCustomSentences->isChecked())
    {
        qDebug() << "adding Custom sentences:";
        for (int counter = 0; counter != addedSentences->count(); ++counter)
        {
            sentences.append(addedSentences->item(counter)->text());
            qDebug() << sentences.last();
        }
    }

}


void DukeWindow::showAboutDialog()
{
    QMessageBox::about(this, tr("About DukeTrayem"),
                       "DukeTrayem v0.3\n"
                       "(C) 2011  JanKusanagi\n\n" +
                       tr("This program puts a Duke Nukem icon "
                          "on your system tray, and will speak "
                          "to you from time to time.\n"
                          "\n"
                          "You can add your own sentences, and use it "
                          "to remind you about things, for instance."));

    // FIXME: show main window, then hide, to avoid closing the program here
    this->show();
    this->hide();
}
