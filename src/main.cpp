/*
 *   This file is part of DukeTrayem
 *   Copyright 2011-2015 JanKusanagi <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include <QApplication>
#include <QtGui>
#include <QTranslator>

#include "dukewindow.h"
int main(int argc, char *argv[])
{
    QApplication dukeApp(argc, argv);

    dukeApp.setApplicationName("DukeTrayem");
    dukeApp.setApplicationVersion("0.3");
    dukeApp.setOrganizationName("JanCoding");
    dukeApp.setOrganizationDomain("jancoding.wordpress.com");

    qDebug() << dukeApp.applicationName() << dukeApp.applicationVersion();

    QTranslator translator;
    QString languageFile;
    // generate a filename for the translation file, based on LANG env variable
    // like duketrayem_en_US
    languageFile = QString(":/translations/duketrayem_%1").arg(qgetenv("LANG").constData());
    // remove anything after the period (".UTF-8" or similar)
    languageFile.remove(QRegExp("\\..*", Qt::CaseInsensitive));
    qDebug() << languageFile;
    translator.load(languageFile); // Load from resources
    dukeApp.installTranslator(&translator);

    DukeWindow dukeWindow;
    dukeWindow.hide();     // redundant, I know ;)

    return dukeApp.exec();
}
