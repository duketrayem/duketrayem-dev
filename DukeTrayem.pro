# DukeTrayem
# by JanKusanagi, 2011
# License: GNU GPL
# -----------------------------
# Project created by QtCreator
# -----------------------------

QT += core gui

## This is for Qt 5
greaterThan(QT_MAJOR_VERSION, 4) QT += widgets

TARGET = duketrayem
TEMPLATE = app
SOURCES += src/main.cpp \
    src/dukewindow.cpp
HEADERS += src/dukewindow.h
OTHER_FILES += README \
    LICENSE
RESOURCES += duketrayem.qrc
TRANSLATIONS += translations/duketrayem_es_ES.ts \
    translations/duketrayem_ca_ES.ts
